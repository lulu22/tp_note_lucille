import requests
from fastapi import FastAPI
from random import randint
import uvicorn 

app = FastAPI(
    title="Connaître le nom d'une musique de mon artiste",
    description="Entrez le nom d'un artiste et vous aurez un nom d'une de ses musiques avec les paroles et le lien youtube",
    version="1.0.0")


@app.get("/")
async def health_check_api():
    return {"health_check":"200"}


@app.get("/random/{artist_name}")
async def nom_musique(artist_name : str) -> dict:
    """
    nom_musique permet de construire ce que l'API va afficher

    Args:
        artist_name (str): Nom de l'artiste

    Returns:
        output (dict) : Dictionnaire avec comme informations le nom de l'artiste, un de ses titres, son url youtube et ses paroles
    """
    title = get_titre_et_url(artist_name)[0]
    output = {
    "artist": artist_name,
    "title": title,
    "suggested_youtube_url": get_titre_et_url(artist_name)[1],
    "lyrics": get_lyrics(artist_name, title)}
    return output


def get_id_artist(artist_name : str) -> str:
    """
    get_id_artist permet d'obtenir l'id d'un artiste à l'aide de son nom

    Args:
        artist_name (str): nom de l'artiste

    Returns:
        id_artiste (str): id de l'artiste
    """
    try:
        requete_id = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php?s={}".format(artist_name))
        id_artiste = requete_id.json()["artists"][0]["idArtist"]
        return id_artiste
    except:
        print("Il n'y a pas d'artiste nommée {} dans notre base de données".format(artist_name))

def get_id_album(artist_name : str) -> str:
    """
    get_id_album renvoie l'id d'un album de l'artiste mis en paramètre

    Args:
        artist_name (str): Nom de l'artiste

    Returns:
        id_album (str): id d'un album de l'artiste. Cet album est choisi aléatoirement. 
    """
    try:
        requete_album = requests.get("https://theaudiodb.com/api/v1/json/2/album.php?i={}".format(get_id_artist(artist_name)))
        id_album = requete_album.json()["album"][randint(0, len(requete_album.json()["album"]) -1)]["idAlbum"]
        return id_album
    except:
        print("L'artiste {} n'a pas d'album rescensé dans notre base de données".format(artist_name))


def get_titre_et_url(artist_name : str) -> tuple:
    """
    get_titre_et_url renvoie un le nom d'un titre et son url youtube à l'aide du nom de l'artiste mis en paramètre

    Args:
        artist_name (str): Nom de l'artiste

    Returns:
        tuple: tuple avec en 1er le nom d'un titre choisi aléatoirement associé à l'artiste et son url youtube
    """
    try:
        requete_titre = requests.get("https://theaudiodb.com/api/v1/json/2/track.php?m={}".format(get_id_album(artist_name)))
        numero_titre = randint(0, len(requete_titre.json()["track"]) -1)
        nom_titre = requete_titre.json()["track"][numero_titre]["strTrack"]
        url_video = requete_titre.json()["track"][numero_titre]["strMusicVid"]
        return (nom_titre, url_video)
    except:
        print("Nous n'avons pas trouvé de titre dans l'album proposé")


def get_lyrics(artist_name : str, title : str) -> str:
    """get_lyrics renvoie les paroles associées au titre de la chanson et à son artiste

    Args:
        artist_name (str): Nom de l'artiste
        title (str): Nom d'un de ses titres

    Returns:
        paroles (str) : Paroles du titre mis en paramètre
    """
    try:
        requete_lyrics = requests.get("https://api.lyrics.ovh/v1/{}/{}".format(artist_name, title))
        paroles = requete_lyrics.json()["lyrics"]
        return paroles
    except:
        print("Il n'y a pas de paroles disponibles pour l'artiste {} et le titre {}".format(artist_name, title))

if __name__ == "__main__":
    uvicorn.run(app)


