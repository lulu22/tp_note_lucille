## Création d'une API musicale 

Cette API permet de proposer une chanson avec son titre, un lien youtube et et ses paroles à partir d'un nom d'artiste. 
### Schéma d'architecture 

```mermaid
graph TD;
  CLIENT --> SERVEUR ;
  SERVEUR --> CLIENT ;
  SERVEUR --> AUDIODB ;
  AUDIODB --> SERVEUR ;
  AUDIODB --> LYRICSOVH;
  LYRICSOVH --> AUDIODB ; 
```

### Installation 
    git clone https://gitlab.com/lulu22/tp_note_lucille.git
    cd tp_note_lucille
    pip install -r requirements.txt 

Attention, si vous possédez python 3.10, il faut que vous tapiez sur votre terminal ce qui suit pour que les tests puissent fonctionner. 

    pip install -U pytest

### Lancement de l'application côté serveur
    
    cd serveur 
    python3 main.py

Changez au besoin l'adresse *url_serveur* stockée dans le fichier .env par l'adresse que vous obtenez dans votre terminal. 

Si vous souhaitez vérifier l'état du serveur, tapez sur une page internet : 

    http://localhost:8000/

Si vous souhaitez obtenir une musique aléatoire et ses paroles d'un artiste, tapez en remplacant {nom_artiste} : 

    http://localhost:8000/random/{nom_artiste}

### Lancement de l'application côté client

Laissez ouvert votre terminal avec l'API qui tourne et ouvrez un deuxième terminal. 

Chargez le fichier contenant vos artistes préférez au format ci dessous (c'est un exemple, vous pouvez y ajouter le nombre d'artiste que vous souhaitez), chargez le dossier client : 

`[{
    "artiste": "daft punk"
},
{
    "artiste":"gloria gaynor"
}]`

Ce fichier doit être au format json. Remplacez le paramètre *nom_fichier_artistes* dans .env avec le nom de votre fichier. 

Enfin, dans votre terminal, tapez : 

    cd tp_note_lucille
    cd client   
    python3 main.py

Après quelques instants, vous aurez la playlist composée de 20 titres de vos artistes favoris. Si vous souhaitez visualiser au mieux cette playlist, un fichier *playlist.csv* s'est créé dans le dossier client. 

### Lancement du test 

Pour mettre en place le test, il faut que votre terminal avec l'API soit toujours ouvert. Dans votre 2ème terminal, tapez : 

    cd client 
    pytest



