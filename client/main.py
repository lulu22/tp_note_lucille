import requests
import json
from dotenv import load_dotenv
import os
from random import *
import pandas

# Chargement des paramètres stockés dans le fichier .env
load_dotenv()

with open(os.getenv("nom_fichier_artistes")) as json_data:
    chanteurs_preferes = json.load(json_data)

def get_info(artist_name : str) -> dict:
    """
    get_info permet d'obtenir les informations sur l'artiste mis en paramètre, c'est-à-dire un dictionnaire avec son nom, un de ses titres, 
    son url youtube et ses paroles

    Args:
        artist_name (str): Nom de l'artiste

    Returns:
        dict: Dictionnaire avec comme informations le nom de l'artiste, un de ses titres, son url youtube et ses paroles
    """
    r = requests.get(os.getenv("url_serveur") + "/random/{}".format(artist_name))
    if r.status_code == 200:
        return r.json()


def get_list_singers(dico_chanteurs_preferes : dict) -> list:
    """
    get_list_singers permet d'obtenir une liste de nom de chanteurs à partir d'un dictionnaire

    Args:
        dico_chanteurs_preferes (dict): dictionnaire contenant les chanteurs pour lesquelles on souhaite avoir une playlist. 

    Returns:
        list_singers (list) : liste des chanteurs 
    """
    list_singers = []
    for i in range(0, len(dico_chanteurs_preferes)):
        list_singers.append(dico_chanteurs_preferes[i]["artiste"])
    return list_singers


def get_playlist(list_singers : list) -> list:
    """
    get_playlist renvoie une liste de dictionnaire avec les informations (un titre, son url youtube et ses paroles) sur les chanteurs que l'on souhaite

    Args:
        list_singers (list): Liste contenant les artistes pour lesquelles on souhaite une playlist

    Returns:
        playlist (list) : Liste de dictionnaire où chaque dictionnaire décrit le nom de l'artiste, un de ses titres, son url et ses paroles
    """
    playlist = []
    for i in range(0, 20):
        titres_playlist = [dico["title"] for dico in playlist]
        info_artist = get_info(choice(list_singers))
        while info_artist["title"] in titres_playlist:
            info_artist = get_info(choice(list_singers))        
        playlist.append(info_artist)
    return playlist

# Affiche dans le terminal la playlist 
playlist = get_playlist((get_list_singers(chanteurs_preferes)))
df_playlist = pandas.DataFrame(playlist)
with pandas.option_context('display.max_rows', None,
                       'display.max_columns', None):   
    print(df_playlist)

# Création d'un fichier csv de la playlist pour avoir les paroles non tronquées
df_playlist.to_csv("playlist.csv")











